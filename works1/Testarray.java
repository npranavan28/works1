package works1;

import java.util.Random;

public class Testarray {

	public static void createRanArray(int size, int range) {

		Random random = new Random();
		int[] randomArray = new int[size];

		for (int r = 0; r < size; r++) {
			randomArray[r] = random.nextInt(range);

		}

		printRandomArray(randomArray);
	}

	private static void printRandomArray(int[] array) {
		for (int element : array) {
			System.out.print(element + ",");

		}
	}
}
